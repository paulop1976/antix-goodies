��          �      �       0     1     B  �   T  G  �  B     $   a  3   �     �     �  F   �     $     5  �  =     �     �  �   �  �  �  I   
  (   O
  7   x
     �
     �
  S   �
     +     D                                
                      	    ADD selected app App .desktop file Choose (or drag and drop to the field below) the .desktop file you want to add to the personal menu \n OR select any other option FPM has no 'graphical' way to allow users to move icons around or delete arbitrary icons.\nIf you click OK, the personal menu configuration file will be opened for editing.\nEach menu icon is identified by a line starting with 'prog' followed by the application name, icon location and the application executable file.\nMove or delete the entire line refering to each personal menu entry.\nNote: Lines starting with # are comments only and will be ignored.\nThere can be empty lines.\nSave any changes and then restart IceWM.\nYou can undo the last change from FPMs 'Restore' button. FTM is programmed to always keep 1 line in the personal menu file! Fast Personal Menu Manager for IceWM No changes were made! Please choose an application. ORGANIZE entries REMOVE last entry This will delete the last entry from your personal menu! Are you sure? UNDO last change Warning Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-26 16:23+0000
Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2020
Language-Team: Swedish (https://www.transifex.com/anticapitalista/teams/10162/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
 LÄGG TILL vald app .desktop-fil för app Välj (eller dra och släpp i fältet nedan) den .desktop-fil du vill lägga till i den personliga menyn \n ELLER ta en annan valmöjlighet FPM har inget 'grafiskt' sätt att låta användarna flytta omkring ikoner eller ta bort slumpmässiga ikoner.\nOm du klickar OK, kommer den personliga meny-konfigurationsfilen att vara öppen för redigering.\nVarje menyikon identifieras av en rad som börjar med 'prog' följt av programnamnet, ikonens plats och programmets körbara fil.\nFlytta eller ta bort hela den rad som refererar till en personlig meny-post.\nAnmärkning: Rader som börjar med # är enbart kommentarer och kommer att ignoreras.\nTomma rader får förekomma.\nSpara ändringar och starta sedan om IceWM.\nDu kan ångra den senaste ändringen med FPMs 'Restore' knapp. FTM är programerad att alltid behålla 1 rad i den personliga menyfilen! Snabb personlig menyhanterare för IceWM Inga ändringar gjordes! Var vänlig välj ett program. ORGANISERA poster TA BORT senaste post Detta kommer att ta bort den sista posten från din personliga meny! Är du säker? TA BORT senaste ändring Varning 