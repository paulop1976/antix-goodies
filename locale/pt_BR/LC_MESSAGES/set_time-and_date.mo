��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  �    <   �     
  -     ;   >  =   z     �     �     �     �  C                          	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-25 22:19+0300
Last-Translator: Not Telling <j.xecure@gmail.com>, 2021
Language-Team: Portuguese (Brazil) (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.3
 Escolha o Fuso Horário (usando as teclas de cursor e Enter) Data: Gerenciador de Configurações de Data e Hora Mova o controle deslizante e posicione sobre a Hora correta Mova o controle deslizante e posicione sobre o Minuto correto Sair Selecione o Fuso Horário Definir a Data Atual Definir a Hora Atual Use o horário do servidor de internet para definir a data e a hora 