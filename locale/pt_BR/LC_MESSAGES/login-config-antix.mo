��          t      �         
             #     8     J     W     o     }  
   �     �  �  �     j     �  +   �     �     �  &   �  !        0     B  "   P                         
                               	    Auto-login Change Change Login Manager Change background Default user Enable numlock at login Login Manager Select Theme Test Theme Test the theme before using it Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-25 18:58+0300
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2021
Language-Team: Portuguese (Brazil) (https://www.transifex.com/antix-linux-community-contributions/teams/120110/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.3
 Autenticação Automática Alterar Alterar o Gerenciador de Início de Sessão Alterar a imagem de fundo Padrão do Usuário Ativar o numlock no início da sessão Gerenciador de Início de Sessão Selecionar o Tema Testar o Tema Testar o tema antes de utilizá-lo 