��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  o    6   �     �  $   �  #   �  %        9     >     W     p  =   �                       	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:04+0000
Last-Translator: Teresio Arrighi, 2021
Language-Team: Italian (https://www.transifex.com/anticapitalista/teams/10162/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=2; plural=(n != 1);
 Scegli il fuso orario (usando i tasti cursore e Invio) Data: Gestione impostazioni di Data e Ora  Sposta il cursore sull'ora corretta Sposta il cursore sul Minuto corretto Esci Seleziona il fuso orario Imposta la data corrente Imposta l'ora corrente Utilizzare l'ora del server Internet per impostare data e ora 