��          t      �         
             #     8     J     W     o     }  
   �     �  �  �     ?     [  ,   c     �     �  *   �  !   �          &  $   5                         
                               	    Auto-login Change Change Login Manager Change background Default user Enable numlock at login Login Manager Select Theme Test Theme Test the theme before using it Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:03+0000
Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2021
Language-Team: French (https://www.transifex.com/anticapitalista/teams/10162/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=2; plural=(n > 1);
 Auto-login / auto-connexion Changer Changer le gestionnaire de connexion / login Changer le fond d'écran Utilisateur par défaut Activer le numlock à la connexion / login Gestionnaire de connexion / login Sélectionner le thème Thème d'essai Tester le thème avant de l'utiliser 