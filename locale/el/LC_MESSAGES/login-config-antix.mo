��          t      �         
             #     8     J     W     o     }  
   �     �  �  �     ?     _  4   l     �  +   �  =   �  )   %     O      m  M   �                         
                               	    Auto-login Change Change Login Manager Change background Default user Enable numlock at login Login Manager Select Theme Test Theme Test the theme before using it Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-29 12:03+0000
Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2021
Language-Team: Greek (https://www.transifex.com/anticapitalista/teams/10162/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
 Αυτόματη σύνδεση Αλλαγή Αλλαγή Διαχειριστή σύνδεσης Αλλαγή φόντου Προεπιλεγμένος χρήστης Ενεργοποίηση numlock κατά τη σύνδεση Διαχειριστής σύνδεσης Επιλογή θέματος Δοκιμάστε το θέμα Δοκιμάστε το θέμα πριν το χρησιμοποιήσετε 