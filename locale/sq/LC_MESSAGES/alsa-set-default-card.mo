��          �      �       H  &   I  *   p     �  !   �     �     �            
   %     0     <  !   K  *   m  �  �  ,   +  +   X     �  !   �      �  $   �       "        3     @     O     e  2   �                             	                   
                 (Could connect to the selected device) (Could not connect to the selected device) Current default is %s No sound cards/devices were found Only one sound card was found. Please Select sound card Quit Sound card set to %s Sound test Test failed Test succeeded Testing sound for up to 6 seconds Would you like to test if the sound works? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-06-07 12:15+0000
Last-Translator: Besnik Bleta <besnik@programeshqip.org>
Language-Team: Albanian (http://www.transifex.com/anticapitalista/antix-development/language/sq/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sq
Plural-Forms: nplurals=2; plural=(n != 1);
 (Mund të lidhet me pajisjen e përzgjedhur) (S’u lidh dot me pajisjen e përzgjedhur) Parazgjedhja aktuale është %s S’u gjetën karta/pajisje zëri U gjet vetëm një kartë zëri. Ju lutemi, Përzgjidhni kartë zëri Dil Si kartë zëri është caktuar %s Provë zëri Prova dështoi Prova qe e suksesshme Provë zëri deri në 6 sekonda Do të dëshironit të provonit nëse zëri punon? 