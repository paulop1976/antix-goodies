��          �      �       H  &   I  *   p     �  !   �     �     �            
   %     0     <  !   K  *   m  �  �  P   :  U   �  0   �  I     D   \  3   �  
   �  0   �          *     I  6   f  \   �                             	                   
                 (Could connect to the selected device) (Could not connect to the selected device) Current default is %s No sound cards/devices were found Only one sound card was found. Please Select sound card Quit Sound card set to %s Sound test Test failed Test succeeded Testing sound for up to 6 seconds Would you like to test if the sound works? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-10-12 12:10+0000
Last-Translator: Peyu Yovev (Пею Йовев) <spacy00001@gmail.com>
Language-Team: Bulgarian (http://www.transifex.com/anticapitalista/antix-development/language/bg/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: bg
Plural-Forms: nplurals=2; plural=(n != 1);
 (Мога да се свържа към избраното устройство) (Не мога са се свържа към избраното устройство) Текущо по подразбиране е %s Не са намерени звукови карти/устройства Само една звукова карта беше открита. Моля изберете звукова карта Изход Зададена е звукова карта %s Тест на звука Теста се провали Теста е успешен Тестване на звука за 6 секунди Искате ли да тествате дали звуковата карта работи? 