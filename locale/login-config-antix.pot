# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-07-17 09:51+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: /home/pc/Documents/login-config-antix-folder/login-config-antix:178
msgid "Login Manager"
msgstr ""

#: /home/pc/Documents/login-config-antix-folder/login-config-antix:179
msgid "Change"
msgstr ""

#: /home/pc/Documents/login-config-antix-folder/login-config-antix:180
msgid "Change Login Manager"
msgstr ""

#: /home/pc/Documents/login-config-antix-folder/login-config-antix:181
msgid "Test Theme"
msgstr ""

#: /home/pc/Documents/login-config-antix-folder/login-config-antix:182
msgid "Test the theme before using it"
msgstr ""

#: /home/pc/Documents/login-config-antix-folder/login-config-antix:206
msgid "Default user"
msgstr ""

#: /home/pc/Documents/login-config-antix-folder/login-config-antix:207
msgid "Auto-login"
msgstr ""

#: /home/pc/Documents/login-config-antix-folder/login-config-antix:208
msgid "Enable numlock at login"
msgstr ""

#: /home/pc/Documents/login-config-antix-folder/login-config-antix:209
msgid "Select Theme"
msgstr ""

#: /home/pc/Documents/login-config-antix-folder/login-config-antix:211
msgid "Change background"
msgstr ""
