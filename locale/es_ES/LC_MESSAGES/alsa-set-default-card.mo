��          �      �       H  &   I  *   p     �  !   �     �     �            
   %     0     <  !   K  *   m  ~  �  .     1   F     x  1   �  (   �  )   �       +   "     N     _     n  %   �  ,   �                             	                   
                 (Could connect to the selected device) (Could not connect to the selected device) Current default is %s No sound cards/devices were found Only one sound card was found. Please Select sound card Quit Sound card set to %s Sound test Test failed Test succeeded Testing sound for up to 6 seconds Would you like to test if the sound works? Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-02-16 14:25+0000
Last-Translator: Casper
Language-Team: Spanish (Spain) (http://www.transifex.com/anticapitalista/antix-development/language/es_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es_ES
Plural-Forms: nplurals=2; plural=(n != 1);
 (se pudo conectar al dispositivo seleccionado) (no se pudo conectar al dispositivo seleccionado) La predeterminada actual es %s No se encontraron tarjetas/dispositivos de sonido Solo se encontró una tarjeta de sonido. Por favor seleccione la tarjeta de sonido Salir La tarjeta de sonido está configurada a %s Prueba de sonido Prueba fallida La prueba fue exitosa Probando el sonido durante 6 segundos ¿Le gustaría probar si el sonido funciona? 