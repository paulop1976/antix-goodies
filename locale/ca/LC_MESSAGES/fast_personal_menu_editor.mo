��          �      �       0     1     B  �   T  G  �  B     $   a  3   �     �     �  F   �     $     5  ~  =     �     �  �   �  ~  t  M   �	  "   A
  8   d
     �
     �
  N   �
       	   3                                
                      	    ADD selected app App .desktop file Choose (or drag and drop to the field below) the .desktop file you want to add to the personal menu \n OR select any other option FPM has no 'graphical' way to allow users to move icons around or delete arbitrary icons.\nIf you click OK, the personal menu configuration file will be opened for editing.\nEach menu icon is identified by a line starting with 'prog' followed by the application name, icon location and the application executable file.\nMove or delete the entire line refering to each personal menu entry.\nNote: Lines starting with # are comments only and will be ignored.\nThere can be empty lines.\nSave any changes and then restart IceWM.\nYou can undo the last change from FPMs 'Restore' button. FTM is programmed to always keep 1 line in the personal menu file! Fast Personal Menu Manager for IceWM No changes were made! Please choose an application. ORGANIZE entries REMOVE last entry This will delete the last entry from your personal menu! Are you sure? UNDO last change Warning Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-02-26 16:23+0000
Last-Translator: Eduard Selma <selma@tinet.cat>, 2021
Language-Team: Catalan (https://www.transifex.com/anticapitalista/teams/10162/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
 Afegeix l'app seleccionada Fitxer App .desktop Trieu (o arrossegueu i deixeu anar) el fitxer .desktop que voleu afegir al vostre menú personal \n O seleccioneu qualsevol altra opció FPM no té un mode "gràfic" per permetre als nous usuaris moure icones o esborrar-ne arbitràriament.\nSi cliqueu OK, s'obrirà la configuració del menú personal per editar-lo.\nCada icona del menú s'identifica amb una línia que comença amb 'prog', seguida del nom de l'aplicació, situació de la icona i el fitxer executable de l'aplicació.\nMogueu o esborreu la línia sencera referida a cada entrada del menú personal.\nNota: Les línies que comencen per # són comentaris i s'ignoraran.\nPoden haver-hi línies buides.\nDeseu els canvis i torneu a arrencar IceWM.\nPodeu desfer el darrer canvi amb el botó "Restaura" de FPM. FPM està programat per mantenir sempre 1 línia al fitxer de menú personal! Gestor personal de menú per IceWM No s'ha fet cap canvi! Si us plau, trieu una aplicació. ORGANITZA entrades ELIMINA la darrera entrada Això esborrarà la darrera entrada del vostre menú personal! N'esteu segurs? DESFÉS el darrer canvi  Atenció  