# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-07-08 16:56+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: \n"

#: set_time-and_date.sh:20
msgid "Manage Date and Time Settings"
msgstr "Manage Date and Time Settings"

#: set_time-and_date.sh:22
msgid "Date:"
msgstr "Date:"

#: set_time-and_date.sh:23
msgid "Set Current Time"
msgstr "Set Current Time"

#: set_time-and_date.sh:24
msgid "Set Current Date"
msgstr "Set Current Date"

#: set_time-and_date.sh:25
msgid "Choose Time Zone (using cursor and enter keys)"
msgstr "Choose Time Zone (using cursor and enter keys)"

#: set_time-and_date.sh:26
msgid "Use Internet Time server to set automaticaly time/date"
msgstr "Use Internet Time server to set automaticaly time/date"

#: set_time-and_date.sh:27
msgid "Move the slider to the correct Hour"
msgstr "Move the slider to the correct Hour"

#: set_time-and_date.sh:28
msgid "Move the slider to the correct Minute"
msgstr "Move the slider to the correct Minute"

#: set_time-and_date.sh:29
msgid "Select Time Zone"
msgstr "Select Time Zone"

#: set_time-and_date.sh:30
msgid "Quit"
msgstr "Quit"
